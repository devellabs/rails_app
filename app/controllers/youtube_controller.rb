require 'google/api_client'

DEVELOPER_KEY = APP_CONFIG['youtube_key']
YOUTUBE_API_SERVICE_NAME = 'youtube'
YOUTUBE_API_VERSION = 'v3'

class YoutubeController < ApplicationController
  include ApplicationHelper


  def search
    headers['Access-Control-Allow-Origin'] = '*'
    headers['Access-Control-Request-Method'] = '*'
  
    client, youtube = get_service
  
    begin
      # Call the search.list method to retrieve results matching the specified query term.
      search_response = client.execute!(
        :api_method => youtube.search.list,
        :parameters => {
          :part => 'snippet',
          :q => params[:q],
          :maxResults => 20
        }
      )
  
      @videos = Hash.new
      @channels = Hash.new
      @playlists = Hash.new
  
      # Add each result to the appropriate list, and then display the lists of matching videos, channels, and playlists.
      search_response.data.items.each do |search_result|
        case search_result.id.kind
          when 'youtube#video'
            @videos[search_result.id.videoId] = search_result.snippet.title
          when 'youtube#channel'
            @channels[search_result.id.channelId] = search_result.snippet.title
          when 'youtube#playlist'
            @playlists[search_result.id.playlistId] = search_result.snippet.title
        end
      end
  
      response = Hash.new
      response['videos'] = @videos
      response['channels'] = @channels
      response['playlists'] = @playlists
      
    rescue Google::APIClient::TransmissionError => e
      puts e.result.body
    end
  
   render :json => ActiveSupport::JSON.encode(response)
  
  end
  
    
  def get_service
    client = Google::APIClient.new(
      :key => DEVELOPER_KEY,
      :authorization => nil,
      :application_name => APP_CONFIG['site_name'],
      :application_version => '1.0.0'
    )
    youtube = client.discovered_api(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION)
  
    return client, youtube
  end
  
  
end
