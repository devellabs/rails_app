Rails app
============

Youtube playlist:
 - ERB
 - AngularJS
 - jQuery
 - Youtube API (JS, Ruby)
 - MySQL


installation:

    cd rails_app

    bundle install

    rake db:create

    rails server
