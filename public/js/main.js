function YoutubeCtrl($scope, $http) {
	$scope.todos = [];

	$scope.addTodo = function() {
		$scope.clearAll();
		
		$('#loading').removeClass('hide');
		$http({method: 'GET', url: '/youtube/search', params: {q: $scope.todoText}}).
	    success(function(data, status, headers, config) {
	    
	    	$('.result').removeClass('hide');
	    	angular.forEach(data.videos, function(videotitle, videoid) {
	    		$scope.todos.push({title:videotitle, id: videoid, done:false});
	    	});
	    	$('#loading').addClass('hide');
	    	
	    }).
	    error(function(data, status, headers, config) {
	      // called asynchronously if an error occurs
	      // or server returns response with an error status.
	    });
		$scope.todoText = '';		
	};

	$scope.selected = function() {
		var count = 0;
		angular.forEach($scope.todos, function(todo) {
			count += todo.done ? 1 : 0;
		});
		return count;
	};

	$scope.clear = function() {
		var oldTodos = $scope.todos;
		$scope.todos = [];
		angular.forEach(oldTodos, function(todo) {
			if (!todo.done) $scope.todos.push(todo);
		});
	};

	$scope.clearAll = function() {
		$scope.todos = [];
		$('.result').addClass('hide');
	};

}




// Fires whenever a player has finished loading
function onPlayerReady(event) {
  event.target.playVideo();
}

// Fires when the player's state changes.
function onPlayerStateChange(event) {
  // Go to the next video after the current one is finished playing
  if (event.data === 0) {
      $.fancybox.next();
  }
}

// The API will call this function when the page has finished downloading the JavaScript for the player API
function onYouTubePlayerAPIReady() {
// Init gallery
  $(".fancybox").attr('rel', 'gallery')
  .fancybox({
    openEffect  : 'none',
    closeEffect : 'none',
    nextEffect  : 'none',
    prevEffect  : 'none',
    padding     : 0,
    margin      : 50,
    beforeShow  : function() {
      // Find the iframe ID
      var id = $.fancybox.inner.find('iframe').attr('id');
      
      // Create video player object and add event listeners
      var player = new YT.Player(id, {
        events: {
          'onReady': onPlayerReady,
          'onStateChange': onPlayerStateChange
        }
      });
    }
  });
}


$(document).ready(function() {

	$(document).on('click', '.runplaylist', function(){
		onYouTubePlayerAPIReady();
		$('ul.playlist a:first').click();
	});

});
